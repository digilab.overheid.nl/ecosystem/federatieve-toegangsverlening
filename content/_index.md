---
title: Introductie
type: docs
weight: 10
---

# {{< figure src="/images/ftv_logo.png" alt="logo" width="50" height="50" >}}Welkom

Dit is de site van het project Federatieve Toegangsverlening, afgekort FTV.

## Over FTV

> FTV stelt een **standaardmethodiek** voor om **toegang tot gegevens en functies** te regelen in een modern **federatief stelsel**.

Denk hierbij aan toegangsregels als: "_Inzage in het inkomen van een burger is toegestaan aan behandelaren van een lopende aanvraag huurtoeslag_".
Deze regel gaat over de uitwisseling tussen de Dienst Toeslagen en de Belastingsdienst, beide deelnemers aan het federatief datastelsel. Beide
partijen _en_ het stelsel spelen een rol in het toezicht houden op toegang. FTV beschrijft een standaardwerkwijze die toezicht uitvoerbaar, sterk en transparant maakt.

De doelen van de standaardmethodiek zijn om toegangscontrole veiliger en fijnmaziger te maken, makkelijker te implementeren en leveranciersonafhankelijkheid te behouden.

In [werkgebied](/docs/4.onderzoek/1.werkgebied) is het onderwerp preciezer omschreven.

### Context

Toegangsverlening, ook wel autorisatie, is één van de technische stelselfuncties in het [Federatief Datastelsel (FDS)](https://federatief.datastelsel.nl/). Dit zijn ze allemaal:

![1.1stelselfuncties.png](/images/1.1stelselfuncties.png)

Toegangsverlening is een aanvulling op o.a. connectiviteit dat zorgt voor een veilige verbinding, en identificatie en authenticatie dat de identiteit van deelnemers
bepaalt en controleert. [Meer over de stelselfuncties](https://federatief.datastelsel.nl/kennisbank/stelselfuncties/).

In [over het project](/docs/5.over_het_project) is meer te lezen over de opdracht en uitvoeringscontext.

## De methodiek

De [methodiek](/docs/1.methodiek) wordt beschreven vanuit verschillende perspectieven:

1. een functionele beschrijving, een landkaart van wat er bij toegangsverlening komt kijken. 
2. een technische architectuur, waarin zowel de componenten van de toegangsoplossing als de plaatsing in het wijdere IT-landschap wordt geschetst.
3. een technische standaard die beschrijft hoe de toegangsoplossing met de omgeving samenwerkt.

Om [aan de slag te gaan](/docs/2.toepassen) met de methodiek bieden we een aantal praktische zaken, zoals een stappenplan, een referentieimplementie en een proefopstelling.

### Status

Een [onderzoek](/docs/4.onderzoek) naar de status van toegangsverlening in techniek en bij de Nederlandse overheid is afgerond.

Op dit moment wordt aan de [technische standaard](/docs/1.methodiek/2.standaard) geschreven. De basis is gereed om ingebouwd te worden
in, en dit wordt nu getest met de FDS connectiviteitsmodule, OpenFSC. Streven is de standaard op 1 juli 2025 aan te bieden voor plaatsing op de 'pas toe of leg uit' lijst.

De referentieimplementate en proefopstelling zijn nog in een vroege fase.

## Meedoen 

[Denk mee](/docs/3.meedoen) met ons door van gedachten te wisselen over de methodiek, individueel of door deel te nemen in de [werkgroep](/docs/3.meedown/werkgroep).

## Contact

Praat mee over FTV op ons [Mattermost](https://digilab.overheid.nl/chat/digilab/channels/federatieve-toegangsverlening) kanaal, vindt de broncode op [Gitlab](https://gitlab.com/digilab.overheid.nl/ecosystem/ftv/federatieve-toegangsverlening), of neem direct contact op met de product owner [Marc de Boer](mailto:marc.deboer@vng.nl).
