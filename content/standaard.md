# Standaard

De lessen uit de implementatie en de consensus die bereikt wordt in de werkgroepen wordt vastgelegd in de [Standaard voor Federatieve Toegangsverlening](https://ftv-standaard-2f223b.gitlab.io/).

# Werkgroepen

- 20 november 2024: [Gegevensminimalisatie](/docs/2.resultaten/7.standaard/1.werkgroepen/gegevensminimalisatie)