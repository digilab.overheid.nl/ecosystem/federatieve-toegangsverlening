---
title: 'ToDo'
weight: 90
---

# ToDo

- [Terminologie](/docs/5.architectuur/resources/terminologie) (WIP)
- NIST attributes document -> samenvatting maken
- Use cases [BRP](/docs/5.architectuur/usecases/brp) (1e opzet)
- Use cases [Kadaster](/docs/5.architectuur/usecases/kadaster) (WIP)
- Use cases [overig](/docs/5.architectuur/usecases/overzicht) (WIP)
- Praktijktest (WIP)
