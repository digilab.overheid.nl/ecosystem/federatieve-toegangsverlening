---
weight: 50
Title: Over het project
---

# Over het project

## Doelstellingen

Federatieve toegangsverlening geeft invulling aan een vernieuwingsvoorstel van het GDI. De opdracht is:

>  **Definieer een standaardmethodiek voor toegangsverlening tot API's**

Er bestaat nu een grote diversiteit aan oplossingen voor toegangsverlening. Dat geeft uitdagingen voor zowel afnemers als aanbieders.

De doelstellingen aan het project, en hoe een standaardmethodiek een oplossing voor de uitdagingen kan zijn, luiden als volgt:

> 1. **Verbeterde technische interoperabiliteit**. API's en hun afnemers zullen beter op elkaar aansluiten als ze dezelfde methodiek gebruiken.
> 2. **Verlaging van de beheerslast**. E&eacute;n enkele methodiek meerdere malen toepassen levert op termijn besparingen in ontwerp, realisatie en beheer.
> 3. **Verhoging van in controle zijn op toegangsbeslissingen**. Door beleid gestandaardiseerd te implementeren wordt het eenvoudiger de juiste toepassing
   ervan te verifiëren en te monitoren.
> 4. **Geeft richting aan het transitiepad naar de eindsituatie**. In de meerjarenvisie worden grote doelen gedefinieerd. Met een concrete invulling
van een deel van de weg daarheen wordt de juiste richting ingezet naar die doelen.

## Planning en status

Het project is in juli 2024 gestart met [fase 1](3.fase1), en afgerond op 28 februari 2025. 

[Fase 2](4.fase2) is op 1 maart gestart en zal tot 31 december 2025 lopen.

## Uitvoeringscontext

### Organisatie

Subsidie voor dit project is verstrekt door de GDI, en wordt uitgevoerd door [VNG Realisatie](https://vng.nl/artikelen/vng-realisatie). 
Voor praktische uitvoering en ondersteuning is aansluiting bij [Digilab](https://digilab.overheid.nl/) gevonden.

Qua organisatorische context valt dit onderwerp onder het programma [Realisatie IBDS](https://realisatieibds.nl/), en daarbinnen onder het [Federatieve datastelsel (FDS)](https://federatief.datastelsel.nl/).

Het primaire doelgebied is daarmee het FDS te voorzien van het aspect toegangsverlening. 
Secundair wordt gekeken of dezelfde of andere methodieken buiten het FDS inzetbaar zijn.

### Architectuur

Als opdracht van de [GDI](https://www.digitaleoverheid.nl/mido/generieke-digitale-infrastructuur-gdi/) valt dit project onder de [MIDO](https://www.digitaleoverheid.nl/mido/) en de [IBDS](https://www.digitaleoverheid.nl/interbestuurlijke-datastrategie/), en is gehouden aan de architectuur-
en andere principes van die stelsels. Daarin in het bijzonder de domeinarchitecturen [Gegevensuitwisseling](https://minbzk.github.io/gdi-gegevensuitwisseling/content/views/Domeinarchitectuur%20gegevensuitwisseling.html) en [Toegang](https://minbzk.github.io/gdi-toegang/content/views/Domeinarchitectuur%20toegang.html).

In de architectuurplaat van FDS geeft FTV invulling aan een van de elementen van de stelselfunctie [Poortwachter](https://federatief.datastelsel.nl/kennisbank/stelselfuncties/#poortwachter).

De methodiek zal de eigenschap 'federatief' dragen, passend in het FDS. Dat wil zeggen dat de oplossing niet gecentraliseerd zal zijn rond een
enkel vertrouwd punt, en ook niet in gescheiden silo's zal plaatsvinden, maar een collectief stelsel van afspraken zal zijn, een samenwerkingsverband.

## Scope

Onze scope bepalen we door in te gaan op een aantal sleutelwoorden uit de doelstelling:

***Definieer*** wil zeggen dat er methodiek wordt voorgesteld, en niet per se gerealiseerd.
Een goede beschrijving van de methode, een breed begrip en draagvlak, en uiteindelijk ook adoptie ervan, dat is het primaire doel.
Er zullen ook tastbare technische resultaten zijn, om aan te tonen dat de methodiek haalbaar, schaalbaar en veilig is.
Daarnaast kan een referentie-implementatie gemaakt worden als educatief instrument en als halffabrikaat, mocht dat in het verloop van het project wenselijk blijven.
Het is expliciet niet de bedoeling om een softwareproduct op te leveren dat in productie genomen kan worden.

Door het aspect ***toegangsverlening*** oftewel autorisatie uit te lichten worden expliciet andere aspecten zoals identificatie, authenticatie en encryptie buiten beschouwing gelaten.
Vanzelfsprekend zijn dit wel cruciale aspecten, die ook spelen bij het uitwisselen van informatie over autorisatie,
en daarmee voorwaardelijk aan verantwoorde gegevensuitwisseling als geheel, maar vallen buiten deze scope.

De toevoeging ***tot API's*** beperkt het project tot geautomatiseerde gegevensuitwisseling.
Het gaat niet over autorisatie via een niet-digitaal medium en ook niet over interacties tussen mens en computer.

Ook beperkt ***tot API's*** de scope tot uitwisseling waarbij minstens twee door API's verbonden systemen betrokken zijn.
Autorisatie binnen een applicatie of dienst, of die niet via API's verloopt heeft wel een grote overlap in methode en techniek,
maar is niet het doel hier.

Tegelijk moet de term 'tot API's' niet te letterlijk genomen worden. Het gaat om de toegang tot datagerichte diensten, 
die via een API tot stand komt.
