---
Title: Meedoen
weight: 30
bookCollapseSection: true
---

# Meedoen

FTV is in wording en op zoek naar medestanders.

## Wat kan jij doen?

- Bekijk de werkwijze en beoordeel hoe deze in jouw API-landschap past
- Denk mee in de [werkgroepen](1.werkgroepen)
- Experimenteer met de referentie-implementatie om inzicht te krijgen in de impact op je werk